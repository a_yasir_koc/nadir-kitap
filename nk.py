import os
import tkinter as tk
from tkinter import messagebox, ttk
import pandas as pd
import NadirKitapFunctions as f
from selenium import webdriver
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
import re


user_agent = UserAgent().random
options = webdriver.ChromeOptions()
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
options.add_argument('--ignore-certificate-errors')
options.add_argument('--test-type')
options.add_argument('test-type')
options.add_argument('--enable-precise-memory-info')
options.add_argument('--disable-popup-blocking')
options.add_argument('--disable-default-apps')
options.add_argument('test-type=browser')
options.add_argument('user-agent=' + user_agent)
options.add_argument('--headless')
options.add_experimental_option('excludeSwitches', ['enable-logging'])


HEIGHT = 750
WIDTH = 1000

interface = tk.Tk()

canvas = tk.Canvas(interface, height=HEIGHT, width=WIDTH)
canvas.pack()

background_frame = tk.Label(interface, bg = '#B4C6E7')
background_frame.place(relwidth = 1, relheight = 1)

book_frame = tk.LabelFrame(interface, bd = 1, bg = '#E2EFDA', text = 'Kitaplar', font = ('Calibri', 13, 'bold'), fg = '#44546A', labelanchor = 'n')
book_frame.place(relx = 0.5, rely = 0.05, relwidth = 0.9, relheight = 0.15, anchor = 'n')

result_frame = tk.LabelFrame(interface, bd = 5, bg = '#E2EFDA', text='Sepetler', font = ('Calibri', 13, 'bold'), fg = '#44546A', labelanchor = 'n')
result_frame.place(relx = 0.5, rely = 0.22, relwidth = 0.9, relheight = 0.75, anchor = 'n')

add_book = tk.Button(book_frame, text = 'Ekle', bg = '#44546A', fg = '#FFFFFF', command = lambda: book_add())
add_book.place(relx = 0.6, rely = 0, relheight = 0.2, relwidth = 0.15)

drop_book = tk.Button(book_frame, text = 'Çıkar', bg = '#44546A', fg = '#FFFFFF', command = lambda: book_drop())
drop_book.place(relx = 0.6, rely = 0.25, relheight = 0.2, relwidth = 0.15)

search_book = tk.Button(book_frame, text = 'Sepet Oluştur', bg = '#44546A', fg = '#FFFFFF', command = lambda: book_search())
search_book.place(relx = 0.6, rely = 0.5, relheight=0.2, relwidth = 0.15)

show_basket = tk.Button(book_frame, text = 'Sepeti Göster', bg = '#44546A', fg = '#FFFFFF', command = lambda: show_basket())
show_basket.place(relx = 0.6, rely = 0.75, relheight=0.2, relwidth = 0.15)

# O an girilenler
add_book2 = tk.Button(book_frame, text = 'Şimdilik Ekle', bg = '#44546A', fg = '#FFFFFF', command = lambda: book_add2())
add_book2.place(relx = 0.8, rely = 0.1, relheight = 0.3, relwidth = 0.15)

search_book2 = tk.Button(book_frame, text = 'Sepet Oluştur', bg = '#44546A', fg = '#FFFFFF', command = lambda: book_search2())
search_book2.place(relx = 0.8, rely = 0.5, relheight=0.3, relwidth = 0.15)

book = tk.Label(book_frame, text = 'Kitap Adı:', bg = '#E2EFDA', fg = '#44546A', font = ('Calibri', 11, 'bold'))
book.place(relx = 0, rely = 0.1, relwidth = 0.15, relheight = 0.3)

author = tk.Label(book_frame, text = 'Yazar Adı:', bg = '#E2EFDA', fg = '#44546A', font = ('Calibri', 11, 'bold'))
author.place(relx = 0, rely = 0.5, relwidth = 0.15, relheight = 0.3)

book_name = tk.StringVar()
book_enter = tk.Entry(book_frame, textvariable = book_name)
book_enter.place(relx = 0.12, rely = 0.1, relwidth = 0.25, relheight = 0.3)

author_name = tk.StringVar()
author_enter = tk.Entry(book_frame, textvariable = author_name)
author_enter.place(relx = 0.12, rely = 0.5, relwidth = 0.25, relheight = 0.3)

style = ttk.Style()
style.configure("mystyle.Treeview", highlightthickness = 0, bd = 0, font = ('Calibri', 11))
style.configure("mystyle.Treeview.Heading", font=('Calibri', 13, 'bold'))
style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])

tree_view = ttk.Treeview(result_frame, style="mystyle.Treeview")
tree_view.place(relx = 0.05, rely = 0.05, relheight = 0.9, relwidth = 0.9)

tree_scroll_x = tk.Scrollbar(result_frame, orient = 'horizontal', command = tree_view.xview)
tree_scroll_y = tk.Scrollbar(result_frame, orient = 'vertical', command = tree_view.yview)
tree_view.configure(xscrollcommand = tree_scroll_x.set, yscrollcommand = tree_scroll_y.set)
tree_scroll_x.pack(side = 'bottom', fill = 'x')
tree_scroll_y.pack(side = 'right', fill = "y")

message_label = tk.Label(book_frame, text = '', bg = '#E2EFDA', font = ('Calibri', 13, 'bold'))
message_label.place(relx = 0.4, rely = 0.3)


def book_add():
	book = book_name.get()
	author = author_name.get()
	if (len(book) > 0) & (len(author) > 0):
		new_book = pd.DataFrame({'Kitap': [book], 'Yazar': [author]})
		BookList = pd.read_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Nadir Kitap.xlsx')
		BookList = pd.concat([BookList, new_book], ignore_index = True)
		BookList.to_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Nadir Kitap.xlsx', index = False)
		message_label.config(text = 'Eklendi', fg = '#A9D08E', font = ('Calibri', 13, 'bold'))
	else:
		message_label.config(text = 'Kitap ve Yazarı yazınız!', fg = '#A9D08E', font = ('Calibri', 12))

def book_add2():
	book = book_name.get()
	author = author_name.get()
	if (len(book) > 0) & (len(author) > 0):
		new_book = pd.DataFrame({'Kitap': [book], 'Yazar': [author]})
		BookList = pd.read_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Nadir Kitap2.xlsx')
		BookList = pd.concat([BookList, new_book], ignore_index = True)
		BookList.to_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Nadir Kitap2.xlsx', index = False)
		message_label.config(text = 'Eklendi', fg = '#A9D08E', font = ('Calibri', 13, 'bold'))
	else:
		message_label.config(text = 'Kitap ve Yazarı yazınız!', fg = '#A9D08E', font = ('Calibri', 12))


def book_drop():
	book = book_name.get()
	author = author_name.get()
	drop_book = pd.DataFrame({'Kitap': [book], 'Yazar': [author]})
	BookList = pd.read_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Nadir Kitap.xlsx')
	BookList = pd.concat([BookList, drop_book], ignore_index = True).drop_duplicates(keep = False)
	BookList.to_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Nadir Kitap.xlsx', index = False)
	message_label.config(text = 'Silindi', fg = '#C00000', font = ('Calibri', 13, 'bold'))


def clear_data():
	tree_view.delete(*tree_view.get_children())
	return None


def book_search():
	BookList = pd.read_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Nadir Kitap.xlsx')
	SearchedBook = f.excel_search(BookList)
	SearchedBook['BookCount'] = SearchedBook.groupby(['SellerLink', 'SellerName']).transform('nunique')['SearchId']
	SearchedBook = SearchedBook[SearchedBook.BookCount > 1].reset_index(drop = True)

	if len(SearchedBook) > 1:
		SearchedBook = SearchedBook.sort_values(['BookCount', 'SellerLink', 'SearchId', 'Price'],
												ascending = [False, True, True, True]).reset_index(drop = True)

		SearchedBook['BookOrder'] = SearchedBook.groupby(['SellerLink', 'SellerName', 'SearchId']).cumcount() + 1
		SearchedBook = SearchedBook[SearchedBook.BookOrder == 1].reset_index(drop = True)

		SearchedBook['TotalPrice'] = SearchedBook.groupby(['SellerLink', 'SellerName']).transform('sum')['Price']
		SearchedBook['TotalPrice'] = round(SearchedBook.TotalPrice.astype('float'), 2)

		Sellers = SearchedBook[['SellerName', 'TotalPrice', 'BookCount', 'SellerLink']]. \
			drop_duplicates().reset_index(drop = True)

		Sellers['SellerName'] = Sellers.apply(lambda row: row.SellerName.upper(), axis = 1)

		ShipmentNotes = list()
		for url in Sellers.SellerLink:
			browser = webdriver.Chrome(executable_path = 'C:/Users/hseym/Downloads/chromedriver.exe',
									   options = options)
			browser.get(url)
			html_code = browser.page_source
			browser.quit()
			soup = BeautifulSoup(html_code, 'lxml')
			shipment_detail = soup.find_all('div', {'class': 'buyer-dtl-text-btm'})
			ship_start = [m.start() for m in re.finditer('<p>', str(shipment_detail))][-1]
			ship_end = [m.start() for m in re.finditer('</p>', str(shipment_detail))][-1]

			ShipmentNotes.append(str(shipment_detail)[ship_start + 3:ship_end])

		Sellers['ShipmentNote'] = ShipmentNotes

		Books = SearchedBook[['SellerLink', 'SearchId', 'Price', 'BookOrder', 'Link']]
		Books['SearchId'] = Books.apply(lambda row: ('   ' + row.SearchId).upper(), axis = 1)
		Books['ShipmentNote'] = ''

		Sellers.columns = ['Sahaf/Kitap', 'Fiyat', 'Kitap Sayısı', 'Link', 'Kargo Notu']
		Sellers = Sellers[['Sahaf/Kitap', 'Fiyat', 'Kitap Sayısı', 'Kargo Notu', 'Link']]

		Books.columns = ['Sahaf Link', 'Sahaf/Kitap', 'Fiyat', 'Kitap Sayısı', 'Link', 'Kargo Notu']
		Books = Books[['Sahaf Link', 'Sahaf/Kitap', 'Fiyat', 'Kitap Sayısı', 'Kargo Notu', 'Link']]

	else:
		Sellers = pd.DataFrame({'Sonuç': ['Sepet oluşrturulamadı.']})
		Books = pd.DataFrame({'Sonuç': ['Sepet oluşrturulamadı.']})

	Sellers.to_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Sahaflar.xlsx', index = False)
	Books.to_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Sepetler.xlsx', index = False)

	load_data()
	message_label.config(text = 'Sepetler Hazırlandı', bg = '#E2EFDA')


def book_search2():
	BookList = pd.read_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Nadir Kitap2.xlsx')
	SearchedBook = f.excel_search(BookList)
	BookList = BookList.drop(index = range(len(BookList)))
	BookList.to_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Nadir Kitap2.xlsx', index = False)

	SearchedBook['BookCount'] = SearchedBook.groupby(['SellerLink', 'SellerName']).transform('nunique')['SearchId']
	SearchedBook = SearchedBook[SearchedBook.BookCount > 1].reset_index(drop = True)

	if len(SearchedBook) > 1:
		SearchedBook = SearchedBook.sort_values(['BookCount', 'SellerLink', 'SearchId', 'Price'],
												ascending = [False, True, True, True]).reset_index(drop = True)

		SearchedBook['BookOrder'] = SearchedBook.groupby(['SellerLink', 'SellerName', 'SearchId']).cumcount() + 1
		SearchedBook = SearchedBook[SearchedBook.BookOrder == 1].reset_index(drop = True)

		SearchedBook['TotalPrice'] = SearchedBook.groupby(['SellerLink', 'SellerName']).transform('sum')['Price']
		SearchedBook['TotalPrice'] = round(SearchedBook.TotalPrice.astype('float'), 2)

		Sellers = SearchedBook[['SellerName', 'TotalPrice', 'BookCount', 'SellerLink']]. \
			drop_duplicates().reset_index(drop = True)

		Sellers['SellerName'] = Sellers.apply(lambda row: row.SellerName.upper(), axis = 1)

		ShipmentNotes = list()
		for url in Sellers.SellerLink:
			browser = webdriver.Chrome(executable_path = 'C:/Users/hseym/Downloads/chromedriver.exe',
									   options = options)
			browser.get(url)
			html_code = browser.page_source
			browser.quit()
			soup = BeautifulSoup(html_code, 'lxml')
			shipment_detail = soup.find_all('div', {'class': 'buyer-dtl-text-btm'})
			ship_start = [m.start() for m in re.finditer('<p>', str(shipment_detail))][-1]
			ship_end = [m.start() for m in re.finditer('</p>', str(shipment_detail))][-1]

			ShipmentNotes.append(str(shipment_detail)[ship_start + 3:ship_end])

		Sellers['ShipmentNote'] = ShipmentNotes

		Books = SearchedBook[['SellerLink', 'SearchId', 'Price', 'BookOrder', 'Link']]
		Books['SearchId'] = Books.apply(lambda row: ('   ' + row.SearchId).upper(), axis = 1)
		Books['ShipmentNote'] = ''

		Sellers.columns = ['Sahaf/Kitap', 'Fiyat', 'Kitap Sayısı', 'Link', 'Kargo Notu']
		Sellers = Sellers[['Sahaf/Kitap', 'Fiyat', 'Kitap Sayısı', 'Kargo Notu', 'Link']]

		Books.columns = ['Sahaf Link', 'Sahaf/Kitap', 'Fiyat', 'Kitap Sayısı', 'Link', 'Kargo Notu']
		Books = Books[['Sahaf Link', 'Sahaf/Kitap', 'Fiyat', 'Kitap Sayısı', 'Kargo Notu', 'Link']]

	else:
		Sellers = pd.DataFrame({'Sonuç': ['Sepet oluşrturulamadı.']})
		Books = pd.DataFrame({'Sonuç': ['Sepet oluşrturulamadı.']})

	Sellers.to_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Sahaflar.xlsx', index = False)
	Books.to_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Sepetler.xlsx', index = False)

	load_data()
	message_label.config(text = 'Sepetler Hazırlandı', bg = '#E2EFDA')

def show_basket():
	load_data()


def load_data():
	sellers_path = 'C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Sahaflar.xlsx'
	sellers = pd.read_excel(sellers_path)

	books_path = 'C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Sepetler.xlsx'
	books = pd.read_excel(books_path)

	clear_data()

	tree_view['columns'] = ('one', 'two', 'three')

	tree_view.column('#0', width = 400, minwidth = 400)
	tree_view.column('one', width = 60, minwidth = 60)
	tree_view.column('two', width = 100, minwidth = 100)
	tree_view.column('three', width = 600, minwidth = 600)

	tree_view.heading('#0', text = 'Sahaf / Kitap', anchor = tk.W)
	tree_view.heading('one', text = 'Fiyat', anchor = tk.W)
	tree_view.heading('two', text = 'Kitap Sayısı', anchor = tk.W)
	tree_view.heading('three', text = 'Kargo Notu', anchor = tk.W)

	rows = sellers.to_numpy().tolist()
	i = 0
	for row in rows:
		i = i + 1
		id = tree_view.insert('', i, text = row[0], values = tuple(row[1:4]))
		book_rows = books[books['Sahaf Link'] == row[4]][sellers.columns[0:4]].to_numpy().tolist()
		for book_row in book_rows:
			tree_view.insert(id, 'end', text = book_row[0], values = book_row[1:])
	return None


interface.mainloop()
