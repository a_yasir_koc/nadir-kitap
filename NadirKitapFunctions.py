from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
from fake_useragent import UserAgent
import math
import re

pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 100)
pd.set_option('display.width', 150)
pd.set_option('display.float_format', lambda x: '%.2f' % x)


user_agent = UserAgent().random
options = webdriver.ChromeOptions()
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
options.add_argument('--ignore-certificate-errors')
options.add_argument('--test-type')
options.add_argument('test-type')
options.add_argument('--enable-precise-memory-info')
options.add_argument('--disable-popup-blocking')
options.add_argument('--disable-default-apps')
options.add_argument('test-type=browser')
options.add_argument('user-agent=' + user_agent)
options.add_argument('--headless')
options.add_experimental_option('excludeSwitches', ['enable-logging'])


"""
ç = %E7
ğ = %F0
ı = %FD
ö = %F6
ş = %FE
ü = %FC
"""


def nadir_search(book, author):
    book_search = book.lower().replace(' ', '+').replace('ç', '%E7').replace('ğ', '%F0').replace('ı', '%FD').\
        replace('ö', '%F6').replace('ş', '%FE').replace('ü', '%FC')
    author_search = author.lower().replace(' ', '+').replace('ç', '%E7').replace('ğ', '%F0').replace('ı', '%FD').\
        replace('ö', '%F6').replace('ş', '%FE').replace('ü', '%FC')

    book_in_link = book.lower().replace(' ', '-').replace('ç', 'c').replace('ğ', 'g').replace('ı', 'i').\
        replace('ö', 'o').replace('ş', 's').replace('ü', 'u')
    author_in_link = author.lower().replace(' ', '-').replace('ç', 'c').replace('ğ', 'g').replace('ı', 'i').\
        replace('ö', 'o').replace('ş', 's').replace('ü', 'u')

    book_link = '-' + author_in_link + '-kitap'

    Results = pd.DataFrame({'SearchId': [],
                            'BookName': [],
                            'AuthorName': [],
                            'Link': [],
                            'SellerName': [],
                            'SellerLink': [],
                            'Price': []})

    page = 1
    url = f"""https://www.nadirkitap.com/kitapara.php?ara=aramayap&kategori=0&kitap_Adi={book_search}&yazar={author_search}&ceviren=&hazirlayan=&siralama=fiyatartan&satici=0&ortakkargo=0&yayin_Evi=&yayin_Yeri=&isbn=&fiyat1=&fiyat2=&tarih1=0&tarih2=0&guzelciltli=0&birincibaski=0&imzali=0&eskiyeni=0&cilt=0&listele=&tip=kitap&dil=0&page={page}"""

    browser = webdriver.Chrome(executable_path = 'C:/Users/hseym/Downloads/chromedriver.exe',
                               options = options)
    browser.get(url)
    html_code = browser.page_source
    browser.quit()
    soup = BeautifulSoup(html_code, 'lxml')

    x = soup.find_all('p', {'class': 'icon no-icon aramap'})
    start = str(x[0]).find('aramap"> ') + len('aramap">')
    end = str(x[0]).find(' sonuç')

    if str(x[0])[start:end] != ' Arama kriterlerinize uygun':
        page_count = math.ceil(int(str(x[0])[start:end]) / 25)

        for p in range(1, page_count + 1):
            if p > 1:
                page = p
                url = f"""https://www.nadirkitap.com/kitapara.php?ara=aramayap&kategori=0&kitap_Adi={book_search}&yazar={author_search}&ceviren=&hazirlayan=&siralama=fiyatartan&satici=0&ortakkargo=0&yayin_Evi=&yayin_Yeri=&isbn=&fiyat1=&fiyat2=&tarih1=0&tarih2=0&guzelciltli=0&birincibaski=0&imzali=0&eskiyeni=0&cilt=0&listele=&tip=kitap&dil=0&page={page}"""
                browser = webdriver.Chrome(executable_path = 'C:/Users/hseym/Downloads/chromedriver.exe',
                                           options = options)
                browser.get(url)
                html_code = browser.page_source
                browser.quit()
                soup = BeautifulSoup(html_code, 'lxml')
            else:
                pass

            counter = 0
            for i in soup.find_all('ul', {'class': 'product-list'})[0].find_all('li'):
                counter = counter + 1
                if counter % 4 == 1:
                    Links = []

                    for link in i.findAll('a'):
                        Links.append(link.get('href'))
                    Links = list(dict.fromkeys(Links))
                    if (book_link in Links[0]) & (book_in_link in Links[0]):
                        book_start = str(Links[0]).find('https://www.nadirkitap.com/') + \
                                     len('https://www.nadirkitap.com/')
                        book_end = str(Links[0]).find(book_link)

                        seller_determinant = ['sahaf1', 'sahaf2', 'sahaf3', 'sahaf4', 'sahaf5', 'sahaf6', 'sahaf7',
                                               'sahaf8', 'sahaf9']
                        seller_start = str(Links[1]).find('https://www.nadirkitap.com/') + \
                                       len('https://www.nadirkitap.com/')
                        for s in seller_determinant:
                            seller_end = str(Links[1]).find(s)
                            if seller_end > -1:
                                break

                        price = i.find_all('div',
                                           {'class': 'col-md-6 col-xs-12 no-padding text-right product-list-price'})
                        price_start = [m.start() for m in re.finditer('>', str(price))][-2] + 1
                        price_end = str(price).find(' TL <')

                        condition = i.find_all('div', {'class': 'ratingCont'})
                        cond_start = [m.start() for m in re.finditer('>', str(condition))][-3]
                        cond_end = [m.start() for m in re.finditer('</span>', str(condition))][-1]

                        condition_list = ['Çok İyi', 'Yeni Gibi', 'Yeni']
                        if str(condition)[cond_start+1:cond_end] in condition_list:
                            Temp = pd.DataFrame({'SearchId': [book + ' / ' + author],
                                                 'BookName': [str(Links[0])[book_start:book_end].replace('-', ' ')],
                                                 'AuthorName': [author],
                                                 'Link': [Links[0]],
                                                 'SellerName': [str(Links[1])[seller_start:seller_end].replace('-', ' ')],
                                                 'SellerLink': [Links[1]],
                                                 'Price': [str(price)[price_start:price_end]]})
                            Results = pd.concat([Results, Temp], ignore_index = True)
                else:
                    pass
    return Results


def excel_search(BookList):
    Results = pd.DataFrame()
    for books in range(len(BookList)):
        book = BookList.loc[books].Kitap
        author = BookList.loc[books].Yazar

        Temp = nadir_search(book, author)
        Results = pd.concat([Results, Temp], ignore_index = True)
        print(book, ' ', author)

    Results['Price'] = Results.apply(lambda row: float(row.Price.replace(',', '.')), axis = 1)
    Results.to_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Kitaplar.xlsx', index = False)

    return Results


def basket_create(SearchedBook):
    SearchedBook['BookCount'] = SearchedBook.groupby(['SellerLink', 'SellerName']).transform('nunique')['SearchId']
    SearchedBook = SearchedBook[SearchedBook.BookCount > 1].reset_index(drop = True)

    if len(SearchedBook) > 1:
        SearchedBook = SearchedBook.sort_values(['BookCount', 'SellerLink', 'SearchId', 'Price'],
                                                ascending = [False, True, True, True]).reset_index(drop = True)

        SearchedBook['BookOrder'] = SearchedBook.groupby(['SellerLink', 'SellerName', 'SearchId']).cumcount() + 1
        SearchedBook = SearchedBook[SearchedBook.BookOrder == 1].reset_index(drop = True)

        SearchedBook['TotalPrice'] = SearchedBook.groupby(['SellerLink', 'SellerName']).transform('sum')['Price']
        SearchedBook['TotalPrice'] = round(SearchedBook.TotalPrice.astype('float'), 2)

        Sellers = SearchedBook[['SellerName', 'TotalPrice', 'BookCount', 'SellerLink']].\
            drop_duplicates().reset_index(drop = True)

        Sellers['SellerName'] = Sellers.apply(lambda row: row.SellerName.upper(), axis = 1)

        ShipmentNotes = list()
        for url in Sellers.SellerLink:
            browser = webdriver.Chrome(executable_path = 'C:/Users/hseym/Downloads/chromedriver.exe', options = options)
            browser.get(url)
            html_code = browser.page_source
            browser.quit()
            soup = BeautifulSoup(html_code, 'lxml')
            shipment_detail = soup.find_all('div', {'class': 'buyer-dtl-text-btm'})
            ship_start = [m.start() for m in re.finditer('<p>', str(shipment_detail))][-1]
            ship_end = [m.start() for m in re.finditer('</p>', str(shipment_detail))][-1]

            ShipmentNotes.append(str(shipment_detail)[ship_start+3:ship_end])

        Sellers['ShipmentNote'] = ShipmentNotes

        Books = SearchedBook[['SellerLink', 'SearchId', 'Price', 'BookOrder', 'Link']]
        Books['SearchId'] = Books.apply(lambda row: ('   ' + row.SearchId).upper(), axis = 1)
        Books['ShipmentNote'] = '-'

        Sellers.columns = ['Sahaf/Kitap', 'Fiyat', 'Kitap Sayısı', 'Link', 'Kargo Notu']
        Sellers = Sellers[['Sahaf/Kitap', 'Fiyat', 'Kitap Sayısı', 'Kargo Notu', 'Link']]

        Books.columns = ['Sahaf Link', 'Sahaf/Kitap', 'Fiyat', 'Kitap Sayısı', 'Link', 'Kargo Notu']
        Books = Books[['Sahaf Link', 'Sahaf/Kitap', 'Fiyat', 'Kitap Sayısı', 'Kargo Notu', 'Link']]

    else:
        Sellers = pd.DataFrame({'Sonuç': ['Sepet oluşrturulamadı.']})
        Books = pd.DataFrame({'Sonuç': ['Sepet oluşrturulamadı.']})

    Sellers.to_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Sahaflar.xlsx', index = False)
    Books.to_excel('C:/Users/hseym/PycharmProjects/Search/nadir-kitap/Sepetler.xlsx', index = False)

    return 1
